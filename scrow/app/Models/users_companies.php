<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class users_companies extends Model
{
    protected $table='params';
    public $timestamps = false;
    protected $primaryKey = 'cod_user_company';
    protected $fillable=["cod_user_company","cod_user","cod_company"];
}
