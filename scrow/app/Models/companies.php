<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class companies extends Model
{
    protected $table='companies';
    public $timestamps = true;
    protected $primaryKey = 'cod_company';
    protected $fillable=["cod_company","nit","name","phone","address","contact_name","email"];
}
