<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profile_module extends Model
{
    protected $table='profile_module';
    public $timestamps = false;
    protected $primaryKey = 'cod_profile_module';
    protected $fillable=["cod_profile_module","cod_profile","cod_module"];
}
