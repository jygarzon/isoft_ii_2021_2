<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transactions extends Model
{
    protected $table='params';
    public $timestamps = true;
    protected $primaryKey = 'cod_trans';
    protected $fillable=["cod_trans","cod_client","cod_supplier","product","description","total_cost"];
}

