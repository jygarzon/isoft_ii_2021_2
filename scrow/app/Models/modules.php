<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class modules extends Model
{
    protected $table='modules';
    public $timestamps = false;
    protected $primaryKey = 'cod_module';
    protected $fillable=["cod_module","name", "route","order"];

    public function profiles(){
        return $this->belongsToMany('App\Models\profiles','profile_module','cod_module','cod_profile');
    }
}
