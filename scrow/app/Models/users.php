<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class users extends Authenticatable
{
    use Notifiable;
    protected $table='users';
    public $timestamps = true;
    protected $primaryKey = 'cod_user';
    protected $fillable=["cod_user","password","fec_maxpass","attempts","name","email","cod_profile","remember_token"];

    protected $hidden = array('password');

    public function setPasswordAttribute($value)
    {
        if( \Hash::needsRehash($value) ) {
            $value = \Hash::make($value);
        }
        $this->attributes['password'] = $value;
    }

    public function profile(){
        return $this->hasOne('App\Models\Profiles', 'cod_profile', 'cod_profile');
    }


}
