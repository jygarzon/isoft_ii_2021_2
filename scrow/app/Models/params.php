<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class params extends Model
{
    protected $table='params';
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $fillable=["id","name","nit","par_chapass","par_dchapa","par_maxatt"];
}
