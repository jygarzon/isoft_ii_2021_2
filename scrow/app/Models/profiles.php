<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profiles extends Model
{
    use HasFactory;
    protected $table='profiles';
    public $timestamps = false;
    protected $primaryKey = 'cod_profile';
    protected $fillable=["cod_profile","name","ind_status"];



    public function modules(){
        return $this->belongsToMany('App\Models\modules', 'profile_module','cod_profile','cod_module')->orderBy('order','ASC');
    }


}
