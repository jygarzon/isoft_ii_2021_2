<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class authController extends Controller
{

    public function login(Request $request){

        $username = $request->input('username');
        $password = $request->input('password');
        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
        if(Auth::guard('user')->attempt(['cod_user' => $username, 'password' => $password]))
        {
            // De ser datos válidos nos mandara a la bienvenida
            return redirect('/dashboard');
        }

        return redirect('/')->with(array(
            'message'=>'Usuario o Contraseña Incorrectos'
        ));
    }

    public function logout(){
        Auth::guard('user')->logout();
        return redirect('/')->with(array(
            'message'=>'Sesión Finalizada'
        ));
    }

}
