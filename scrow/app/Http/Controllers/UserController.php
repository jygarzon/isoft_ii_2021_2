<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\users;
use App\Models\profiles;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = users::get();
        return view('masters/users/list')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profiles = profiles::get();
        return view('masters/users/create')->with('profiles', $profiles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [

            'cod_user' => 'required',
            'name' => 'required',
            'email'    => 'required|email',
            'cod_profile' => 'required',
        ]);

        if ($v->fails())
        {
            return redirect('/user')->with(array(
                'message'=>'No se pudo crear el usuario. Valide la informacion ingresada.',
                'color' => '#C20917',
                'icon' => 'fa fa-times'
            ));
        }

        $dia = date("Y-m-d");
        if($request->input('dayschange')!=''){
            $fec_maxpassd = strtotime($dia."+ ".$request->input('dayschange')." days");
            $fec_maxpass = date("Y-m-d", strtotime($fec_maxpassd));
        }else{
            $fec_maxpass = NULL;
        }

        $usuario = new users;
        $usuario->cod_user = $request->input('cod_user');
        $usuario->password = bcrypt($request->input('password1'));
        $usuario->fec_maxpass = $fec_maxpass;
        $usuario->attempts = 0;
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $usuario->cod_profile = $request->input('cod_profile');
        $usuario->save();
        return redirect('/user')->with(array(
            'message'=>'Usuario Creado',
            'color' => '#349B00',
            'icon' => 'fa fa-check-circle'
        ));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario=users::find($id);
        $profiles = profiles::get();
        return view('masters/users/edit')->with('usuario', $usuario)->with('profiles', $profiles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $v = \Validator::make($request->all(), [
            'name' => 'required',
            'email'    => 'required|email',
            'cod_profile' => 'required',
        ]);

        if ($v->fails())
        {
            return redirect('/user')->with(array(
                'message'=>'No se pudo actualizar el usuario. Valide la informacion ingresada.',
                'color' => '#C20917',
                'icon' => 'fa fa-times'
            ));
        }

        $usuario=users::find($id);
        if($request->input('password1')!=''){
            $usuario->password = bcrypt($request->input('password1'));
        }
        $usuario->fec_maxpass = $request->input('dayschange');
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $usuario->cod_profile = $request->input('cod_profile');
        $usuario->save();
        return redirect('/user')->with(array(
            'message'=>'Cambios realizados',
            'color' => '#349B00',
            'icon' => 'fa fa-check-circle'
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario=users::find($id);
        $usuario->delete();

        return redirect('/user')->with(array(
            'message'=>'Usuario Eliminado',
            'color' => '#349B00',
            'icon' => 'fa fa-check-circle'
        ));
    }
}
