<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('profiles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        $profiles = [
            ['cod_profile' => 1, 'name' => 'Administrador', 'ind_status' => 1]
        ];

        foreach($profiles as $profile){
            DB::table('profiles')->insert($profile);
        }
    }
}
