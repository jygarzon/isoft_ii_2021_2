<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id('cod_trans');
            $table->unsignedBigInteger('cod_client')->unsigned();
            $table->unsignedBigInteger('cod_supplier')->unsigned();
            $table->string('product',255)->nullable();
            $table->longText('description')->nullable();
            $table->double('total_cost', 8, 6);
            $table->timestamps();
            $table->foreign('cod_client')->references('cod_company')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cod_supplier')->references('cod_company')->on('companies')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
