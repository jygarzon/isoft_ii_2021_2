<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_companies', function (Blueprint $table) {
            $table->id('cod_user_company');
            $table->unsignedBigInteger('cod_user')->unsigned();
            $table->unsignedBigInteger('cod_company')->unsigned();
            $table->foreign('cod_user')->references('cod_user')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cod_company')->references('cod_company')->on('companies')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_companies');
    }
}
