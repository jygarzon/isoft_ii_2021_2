<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id("cod_user");
            $table->string('password',255)->nullable();
            $table->date('fec_maxpass');
            $table->tinyInteger('attempts');
            $table->string('name',100)->nullable();
            $table->string('email',100)->nullable();
            $table->unsignedBigInteger('cod_profile')->unsigned();
            $table->string('remember_token',255)->nullable();
            $table->timestamps();
            $table->foreign('cod_profile')->references('cod_profile')->on('profiles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
