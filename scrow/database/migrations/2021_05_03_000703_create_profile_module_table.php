<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_module', function (Blueprint $table) {
            $table->id('cod_profile_module');
            $table->unsignedBigInteger('cod_profile')->unsigned();
            $table->unsignedBigInteger('cod_module')->unsigned();
            $table->foreign('cod_profile')->references('cod_profile')->on('profiles')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cod_module')->references('cod_module')->on('modules')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_module');
    }
}
