@extends("layouts.panel")

@section('head')
<title>Usuarios</title>
@endsection


@section('content')
<div class="card">
    <div class="card-body">
        @if(Session::has('message'))
            <div class="card-header messagepost mb-3" style="background-color:{{ Session::get('color') }};">
                <center><h6 style="color:#fff;"><i class="{{ Session::get('icon') }}"></i> {{ Session::get('message') }}</h6></center>
            </div>
        @endif
        <div class="row">
            <div class="col-md-8">
                <h4 class="mb-3">Listado de Usuarios</h4>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ url('/create-user') }}" class="btn btn-primary btn-fw mr-2 text-white">Crear Nuevo Usuario</a>
            </div>
        </div>

        <table class="table mt-3">
        <thead>
                <tr>
                    <th>Username</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Perfil</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->cod_user}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        <label class="badge badge-info">{{$user->profile->name}}</label>
                    </td>
                    <td>
                        <a href="{{ url('/edit-user/'.$user->cod_user) }}" class="btn btn-icons btn-rounded btn-info text-white">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="{{ url('/delete-user/'.$user->cod_user) }}" class="btn btn-icons btn-rounded btn-danger text-white">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
