@extends("layouts.panel")

@section('head')
<title>Editar Usuario</title>
@endsection


@section('content')
<div class="row page-title-header" style="margin-bottom: 10px;">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title" style="width: 170px;"> Editar Usuario -> </h4>
            <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
                <ul class="quick-links">
                    <li><a href="{{ url('/user') }}">Listado</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <form method="POST" action="{{url('/update-user/'.$usuario->cod_user)}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12">
                    <h4>Edición de Usuario</h4>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label>Username:</label>
                        <input type="text" class="form-control form-control-lg" placeholder="Username" disabled readonly name="cod_user" value="{{$usuario->cod_user}}">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control form-control-lg" placeholder="Nombre" required name="name" value="{{$usuario->name}}">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="text" class="form-control form-control-lg" placeholder="Email" required name="email" value="{{$usuario->email}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Perfil</label>
                        <select class="form-control form-control-lg" id="exampleFormControlSelect1" name="cod_profile">
                            @foreach($profiles as $profile)
                            <option value="{{$profile->cod_profile}}">{{$profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Fec c. de contraseña:</label>
                        <input type="date" class="form-control form-control-lg" name="dayschange" value="{{$usuario->fec_maxpass}}">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Contraseña:</label>
                        <input type="password" class="form-control form-control-lg" placeholder="Contraseña" name="password1">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Confirme Contraseña:</label>
                        <input type="password" class="form-control form-control-lg" placeholder="Confirme Contraseña" name="password2">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary btn-fw">Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
