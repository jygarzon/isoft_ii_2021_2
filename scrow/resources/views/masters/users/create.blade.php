@extends("layouts.panel")

@section('head')
<title>Crear Usuario</title>
@endsection


@section('content')
<div class="row page-title-header" style="margin-bottom: 10px;">
    <div class="col-12">
        <div class="page-header">
            <h4 class="page-title" style="width: 170px;"> Crear Usuario -> </h4>
            <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
                <ul class="quick-links">
                    <li><a href="{{ url('/user') }}">Listado</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <form method="POST" action="{{url('/store-user')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12">
                    <h4>Crear Nuevo Usuario</h4>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label>Username:</label>
                        <input type="text" class="form-control form-control-lg" placeholder="Username" required name="cod_user">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control form-control-lg" placeholder="Nombre" required name="name">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="text" class="form-control form-control-lg" placeholder="Email" required name="email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Perfil</label>
                        <select class="form-control form-control-lg" id="exampleFormControlSelect1" name="cod_profile">
                            @foreach($profiles as $profile)
                            <option value="{{$profile->cod_profile}}">{{$profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label>Dias C. Contraseña:</label>
                        <input type="text" class="form-control form-control-lg" placeholder="Dias" name="dayschange">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Contraseña:</label>
                        <input type="password" class="form-control form-control-lg" placeholder="Contraseña" required name="password1">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Confirme Contraseña:</label>
                        <input type="password" class="form-control form-control-lg" placeholder="Confirme Contraseña" required name="password2">
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary btn-fw">Crear</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
